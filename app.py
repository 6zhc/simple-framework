from flask import Flask, render_template, request
from flask import jsonify
from SlideManifest import SlideManifest
import dzi_online
import copy
import openslide


app = Flask(__name__)
dzi_online.add_dzi_sever(app)
manifest = SlideManifest('manifest.txt')


@app.route('/dashboard')
def dashboard():
    return render_template('dashboard.html')


@app.route('/graph')
def graph():
    return render_template('graph.html')


@app.route('/slide')
def slide():
    slide_id = request.args.get('slide_id', default=1, type=int)
    slide_url = "/dzi_online/data/" + manifest.get_id(slide_id) + '/' + manifest.get_filename(slide_id) + ".dzi"
    return render_template('slide.html',slide_url = slide_url)


@app.route('/')
@app.route('/table')
def table():
    return render_template('table.html')


@app.route('/data')
def data():
    s = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 10]
    t = {}
    t['x'] = copy.deepcopy(s)
    t['y'] = copy.deepcopy(s)
    t['y'][5]=20
    return jsonify(t)


@app.route('/manifest_table_data')
def table_data():
    data = []
    for i in range(1, manifest.get_total_size()):
        temp = []
        temp.append(i)
        temp.append(manifest.get_id(i))
        temp.append(manifest.get_filename(i))

        try:
            file_path = "data/" + manifest.get_id(i) + '/' + manifest.get_filename(i)
            dimensions = openslide.open_slide(file_path).dimensions
            temp.append(str(dimensions[0]).rjust(6, '_') + ' * ' + str(dimensions[1]).rjust(6, '_'))
            temp.append('<a href="/slide?slide_id=' + str(i) + '"target="_blank">Viewing</a>')
        except:
            temp.append('No SVS file')
            temp.append('<a "target="_blank">Viewing</a>')

        data.append(temp)
    return jsonify(data)




# app.run(debug=True)
